﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameMenu : MonoBehaviour {
	
	public TextMesh modeText;
	private Renderer rend;
	private static int gameMode = 0; //0 : adventure, 1 : runner

	void Start(){
		rend = GetComponent<Renderer> ();
	}

	void OnMouseEnter(){
		rend.material.color = Color.blue;
	}

	void OnMouseExit(){
		rend.material.color = Color.white;
	}

	void OnMouseUp(){
		if (rend.name == "rightArrow" || rend.name == "leftArrow") {
			if (gameMode == 0) {
				gameMode = 1;
				modeText.text = "       Hell";
				modeText.alignment = TextAlignment.Center;
            } else if (gameMode == 1) {
                gameMode = 0;
				modeText.text = "Adventure";
				modeText.alignment = TextAlignment.Center;
			}
		} else if(rend.name == "startText") {
			if (gameMode == 0) {
                gameMode = 0;
                SceneManager.LoadScene ("scene1");
            }
            if (gameMode == 1)
            {
                gameMode = 0;
                SceneManager.LoadScene("Dvorak");
            }
        }
	}
}
