﻿#pragma strict


var activate : boolean;
var mainCamera : GameObject;
private var cameraScript : CameraC;

function Awake(){
	cameraScript = mainCamera.GetComponent(CameraC);
}

function Update () {

}

function OnTriggerEnter(Trigger : Collider){
	if(Trigger.tag == 'Player'){
		if(activate){
			cameraScript.enable = true;
		}else{
			cameraScript.enable = false;
		}
	}
}