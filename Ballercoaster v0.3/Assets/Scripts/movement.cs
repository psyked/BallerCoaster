﻿/*using UnityEngine;
using System.Collections;

public class movement : MonoBehaviour {

    private Rigidbody rb;
    public float speed;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
		rb.maxAngularVelocity = 25;
    }
    
    void Update ()
    {
        float turn = Input.GetAxis("Horizontal");
        float move = Input.GetAxis("Vertical");
		Vector3 movement = new Vector3 (move*speed, 0, -turn*speed);
		movement = Camera.main.transform.TransformDirection (movement);
		rb.AddTorque(movement);
    }

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.CompareTag ("Whomp")) {
			rb.position = new Vector3 (0, -46, 0);
		}
	}
}*/


using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class movement : MonoBehaviour {

    private bool stopTimer = false;
	private Rigidbody rb;
	private bool isEnded = false;
	public float forwardSpeed;
	public float sideSpeed;
	public Text timeText;
	public Text victoryText;
	private bool redOrGreen = false;
	public float slowingSpeed;
	public Transform myCube;

    private float timeElapsed;

    void Awake()
    {
        timeElapsed = Time.realtimeSinceStartup;
    }



    void LateUpdate()
    {
        float f = Time.realtimeSinceStartup - timeElapsed;
        string s = f.ToString();
        int seconde = (int)f;
        timeText.text = s.Substring(0, 3 + seconde.ToString().Length);
    }

    void Start()
	{
		rb = GetComponent<Rigidbody>();
		rb.maxAngularVelocity = 25;
		changePlatforms ();
	}

	void Update ()
	{
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("GameMenu");
            Debug.Log("coucou");
        }
		if (!isEnded) {
			float turn = Input.GetAxis ("Horizontal");
			float move = Input.GetAxis ("Vertical");
			Vector3 movement = new Vector3 (0, 0, 0);
			Vector3 actual = rb.angularVelocity;
			actual = myCube.transform.TransformDirection (actual);
			if (turn == 0) {
				actual.y = -actual.y * slowingSpeed;
			} else {
				actual.y = 0;
			}
			if (move == 0) {
				actual.x = -actual.x * slowingSpeed;
			} else {
				actual.x = 0;
			}
			actual.z = -actual.z * slowingSpeed;
			movement = new Vector3 (actual.x, actual.y, actual.z);
			movement = myCube.TransformDirection (movement);
			rb.AddTorque (movement);

			movement = new Vector3 (move * forwardSpeed, 0, -turn * sideSpeed);
			movement = Camera.main.transform.TransformDirection (movement);
			rb.AddTorque (movement);

			if (Input.GetKeyDown (KeyCode.Space)) {
				changePlatforms ();
			}
		}
        /*
        if(!stopTimer)
        {
            string s = Time.time.ToString();
            int seconde = (int)Time.time;
            timeText.text = s.Substring(0, 3 + seconde.ToString().Length);
        }*/
    }



	void OnTriggerEnter(Collider other) {
		if (other.gameObject.CompareTag ("Whomp")) {
			rb.position = new Vector3 (0, -80, 0);
		} else{
			if(other.gameObject.CompareTag("VictoryGem")){
				other.gameObject.SetActive (false);
				victoryText.text = "Victory !";
				rb.velocity = new Vector3 (0, 0, 0);
				isEnded = true;
                stopTimer = true;
			}
		}
	}

	void changePlatforms(){
		GameObject[] gps = GameObject.FindGameObjectsWithTag ("GreenPlatform");
		GameObject[] rps = GameObject.FindGameObjectsWithTag ("RedPlatform");

		foreach (GameObject gp in gps) {
			Renderer gpR = gp.GetComponent<Renderer> ();
			gpR.enabled = redOrGreen;
			BoxCollider gpB = gp.GetComponent<BoxCollider> ();
			gpB.enabled = redOrGreen;
		}

		foreach (GameObject rp in rps) {
			Renderer rpR = rp.GetComponent<Renderer> ();
			rpR.enabled = !redOrGreen;
			BoxCollider rpB = rp.GetComponent<BoxCollider> ();
			rpB.enabled = !redOrGreen;
		}
		redOrGreen = !redOrGreen;
	}


}
