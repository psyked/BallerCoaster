﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class respawner : MonoBehaviour {
    public bool isActivated = false;
	public Text deathCountText;
    public float limite = 0;
    public Rigidbody player;

	private int deathCount = 0;

	void Start () {
		deathCountText.text = "Death count : " + deathCount.ToString ();
		if(isActivated)
        {
            player.position = transform.position;
        }
	}
	
	void Update ()
    {
        if (isActivated)
        {
            if(player.position.y<limite)
            {
				deathCount++;
				deathCountText.text = "Death count : " + deathCount.ToString ();
                player.position = transform.position;
				player.velocity = Vector3.zero;
				player.angularVelocity = Vector3.zero;
            }
            
        }
    }
}
