﻿using UnityEngine;
using System.Collections;

public class translation : MonoBehaviour {
    private Rigidbody rb;
    public float xMin, xMax, yMin, yMax, zMin, zMax, xSpeed, ySpeed, zSpeed = 0;
    private float xDir, yDir, zDir = 1;

	void Start () {
		rb = GetComponent<Rigidbody>();
	}


	void Update ()
    {
        // /!\ position relative to the parent used
	    if(rb.position.x >= xMax){ xDir = -1;}
        if (rb.position.x <= xMin) { xDir = 1; }
        
        if (rb.position.y >= yMax) { yDir = -1; }
        if (rb.position.y <= yMin) { yDir = 1; }

        if (rb.position.z >= zMax) { zDir = -1; }
        if (rb.position.z <= zMin) { zDir = 1; }

        rb.transform.Translate(new Vector3(xDir * xSpeed, yDir*ySpeed, zDir * zSpeed));
    }
}
