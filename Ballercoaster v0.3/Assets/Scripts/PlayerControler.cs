﻿using UnityEngine;
using System.Collections;

public class PlayerControler : MonoBehaviour {

	public float speed;
	private Rigidbody rb;

	void Start () {
		rb = GetComponent<Rigidbody>();
	}

	void FixedUpdate () {
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);

		movement = Camera.main.transform.TransformDirection (movement);
		rb.AddForce (movement * speed);
	}
		
	/*void OnTriggerEnter(Collider other) {
		if (other.gameObject.CompareTag ("Whomp")) {
			rb.position = new Vector3 (0, -46, 0);
		}
	}*/
}
