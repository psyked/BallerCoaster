﻿using UnityEngine;
using System.Collections;

public class CameraControler : MonoBehaviour {

	/*This camera smoothes out rotation around the y-axis and height for objects that roll which makes it difficult to just use the forward Vector.
		Horizontal Distance to the target is always fixed.


		There are many different ways to smooth the rotation but doing it this way gives you a lot of control over how the camera behaves.


		For every of those smoothed values we calculate the wanted value and the current value.
		Then we smooth it using the Lerp function.
			Then we apply the smoothed values to the transform's position.
			*/
			//#pragma strict;

			// The target we are following
	public Transform target;
	public Rigidbody targetRB;
	public bool enable;
	public Vector3 offset;

	// The distance in the x-z plane to the target
	public float distance = 3;
	// the height we want the camera to be above the target
	public float height = 3;
	// How much we 
	public float heightDamping = 2;
	public float rotationDamping = 3;


	void Start(){
		offset = transform.position - target.transform.position;
	}

	void LateUpdate(){
		// Early out if we don't have a target
		if (!target || !targetRB)
			return;

		if(!enable){
			//Take the target's velocity, remove the y and normalize it.  The final step is probably unneeded but makes it easier to extend.
			Vector3 direction = targetRB.velocity;
			direction.y = 0;
			direction = direction.normalized;

			//Get the desired rotation from the ball's velocity.
			Quaternion wantedRotation = Quaternion.LookRotation(direction); 
			float wantedRotationAngle = wantedRotation.eulerAngles.y;
			float wantedHeight = target.position.y + height;

			//Our current rotation is the ball's position minus our own.    
			Quaternion currentRotation = Quaternion.LookRotation(target.position - transform.position);   
			float currentRotationAngle = currentRotation.eulerAngles.y;
			float currentHeight = transform.position.y;

			// Damp the rotation around the y-axis
			currentRotationAngle = Mathf.LerpAngle (currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime);

			// Damp the height
			currentHeight = Mathf.Lerp (currentHeight, wantedHeight, heightDamping * Time.deltaTime);

			// Convert the angle into a rotation
			Quaternion finalRotation = Quaternion.Euler (0, currentRotationAngle, 0);

			// Set the position of the camera on the x-z plane to:
			// distance meters behind the target
			transform.position = target.position;
			transform.position -= finalRotation * Vector3.forward * distance;

			// Set the height of the camera
			transform.position += new Vector3(0,currentHeight,0);
			// Always look at the target
			transform.LookAt (target);

		}else if(enable){
			transform.position = target.transform.position +  offset + new Vector3(0,5,-4);
			//transform.Rotate(new Vector3(45,0,0));
			transform.LookAt (target);
		}
	}
}
