﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Checkpoints : MonoBehaviour
{
	public GameObject respawn;
	public Text checkPointText;

	void OnTriggerEnter(Collider cld)
	{
		if (respawn.transform.position != transform.position) {
			respawn.transform.position = transform.position;
			StartCoroutine (tempo ());
		}
	}

	IEnumerator tempo(){
		checkPointText.text = "Checkpoint !";
		yield return new WaitForSeconds (1);
		checkPointText.text = "";
	}
}
