﻿using UnityEngine;
using System.Collections;

public class createNewPlane : MonoBehaviour
{
    public GameObject myParent;
    public GameObject myGrandpa;
    private bool created = false;
    public TextMesh text;

    void Update()
    {
        GameObject cld = GameObject.FindGameObjectWithTag("mySphere");

        if (transform.InverseTransformVector(cld.transform.position).x > transform.position.x)
        {
            string a = text.text;
            if(a != "hell")
            {
                int A = int.Parse(a);
                A++;
                text.text = A.ToString();
            }            

            if (!created)
            {                
                GameObject newParent = Instantiate<GameObject>(myParent);
                newParent.transform.SetParent(myGrandpa.transform);
                newParent.transform.localRotation = myParent.transform.localRotation;
                Vector3 me = myParent.transform.localPosition;
                me.x += 100;
                newParent.transform.localPosition = me;
                newParent.transform.localScale = myParent.transform.localScale;
                created = true;
            }
        }
    }
}
