﻿using UnityEngine;
using System.Collections;

public class myBouncer : MonoBehaviour {
    public bool preventXBounce, preventYBounce, preventZBounce;
    public float multiply = 1;
    void OnTriggerExit(Collider cld)
    {
        Rigidbody rb = cld.GetComponent<Rigidbody>();
        Vector3 vel = transform.InverseTransformVector(rb.velocity * multiply);
        if (preventXBounce) { vel.x=0; }
        if (preventYBounce) { vel.y = 0; }
        if (preventZBounce) { vel.z = 0; }
        cld.GetComponent<Rigidbody>().velocity = vel;
    }
}
