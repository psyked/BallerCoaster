﻿using UnityEngine;
using System.Collections;

public class punch : MonoBehaviour
{
    public float x, y, z, time;
    public TextMesh text;
    private bool punched = false;

    public void  punchPlayer()
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.AddForce(new Vector3(x, y, z));
        text.text = "0";
    }
        public void Update()
    {
        if (Time.timeSinceLevelLoad >= time && !punched)
        {
            punchPlayer(); punched = true;            
        }
    }

}
