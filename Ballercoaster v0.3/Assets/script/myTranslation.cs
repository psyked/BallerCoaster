﻿using UnityEngine;
using System.Collections;

public class myTranslation : MonoBehaviour {
    public float xMin, xMax, yMin, yMax, zMin, zMax, xSpeed, ySpeed, zSpeed = 0;
    private float xDir, yDir, zDir = 1;

    void Update ()
    {
        // /!\ position relative to the parent used

	    if(transform.localPosition.x >= xMax){ xDir = -1;}
        if (transform.localPosition.x <= xMin) { xDir = 1; }
        
        if (transform.localPosition.y >= yMax) { yDir = -1; }
        if (transform.localPosition.y <= yMin) { yDir = 1; }

        if (transform.localPosition.z >= zMax) { zDir = -1; }
        if (transform.localPosition.z <= zMin) { zDir = 1; }

        transform.Translate(transform.InverseTransformVector(new Vector3(xDir * xSpeed, yDir*ySpeed, zDir * zSpeed)));
    }
}
