﻿using UnityEngine;
using System.Collections;

public class myBoostPlayer : MonoBehaviour
{
    public float X, Y, Z;

    void OnTriggerEnter(Collider cld)
    {
        Rigidbody player = cld.GetComponent<Rigidbody>();
        player.AddForce(transform.TransformVector(new Vector3(X, Y, Z)));        
    }
}
