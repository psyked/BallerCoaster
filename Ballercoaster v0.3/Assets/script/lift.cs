﻿using UnityEngine;
using System.Collections;

public class lift : MonoBehaviour {
    public float ySpeed,yMax = 0;

    void Update ()
    {
        if (transform.localPosition.y < yMax)
        {
            transform.Translate(transform.InverseTransformVector(new Vector3(0, ySpeed,0)));
        }
        
    }
}
