﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class myMovement : MonoBehaviour {

    public Transform myCube;
    private Rigidbody rb;
    public float forwardSpeed;
    public float sideSpeed;
    public bool turnY;
    public float slowX, slowY;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
		rb.maxAngularVelocity = 50;
    }
    
    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("GameMenu");
        }
        float turn = Input.GetAxis("Horizontal");
        float move = Input.GetAxis("Vertical");

        if(rb.velocity.x > 45)
        {
            rb.AddForce(new Vector3(-90,0,0));
        }
        
        Vector3 movement = new Vector3(0,0,0);
        Vector3 actual = rb.angularVelocity;
        actual = myCube.transform.TransformDirection(actual);
        if (turn == 0)
        {
            actual.y = -actual.y*slowY;
        }
        else
        {
            actual.y = 0;
        }
        if (move == 0)
        {
            actual.x =  -actual.x*slowX;
        }
        else
        {
            actual.x = 0;
        }
        actual.z = -actual.z * 0.3f;
        movement = new Vector3(actual.x,actual.y,actual.z);
        movement = myCube.TransformDirection(movement);
        rb.AddTorque(movement);
        float turnByY = 0;
        float turnByZ = 0;
        if (turnY)
        {
            turnByY = 1.0f;
        }
        else { turnByZ = 1.0f; }
        movement = new Vector3(move * forwardSpeed,-turn * sideSpeed * turnByY, -turn * sideSpeed * turnByZ);
        
		movement = Camera.main.transform.TransformDirection (movement);
        rb.AddTorque(movement);
    }
  }