﻿using UnityEngine;
using System.Collections;

public class myRespawn : MonoBehaviour {
    public bool isActivated, punchAtStart = false;
    public Transform relativ = null;
    public float limite = 0;
    public Rigidbody player;

	void Start () {
        if(isActivated)
        {
            player.position = transform.position;
            if (punchAtStart) { player.GetComponent<punch>().punchPlayer(); }
        }
	
	}
	
	void Update ()
    {
        if (isActivated)
        {
            if(relativ != null)
            {
                if (relativ.InverseTransformVector(player.position).y < limite)
                {
                    player.position = transform.position;
                    player.velocity = new Vector3(0, 0, 0);
                    player.angularVelocity = new Vector3(0, 0, 0);
                    player.GetComponent<punch>().punchPlayer();
                }
                return;
            }
            if(player.position.y < limite)
            {
                player.position = transform.position;
                player.velocity = new Vector3(0, 0, 0);
                player.angularVelocity = new Vector3(0, 0, 0);
            }
            
        }
    }
}
